program Navifarm;

uses
  Vcl.Forms,
  frmMain in 'src\frmMain.pas' {MainForm},
  Helper in 'src\Helper.pas',
  FileManager in 'src\FileManager.pas',
  frmProgress in 'src\frmProgress.pas' {ProgressForm},
  DataModule in 'src\DataModule.pas' {dmod: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(Tdmod, dmod);
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.
