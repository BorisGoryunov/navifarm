object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = 'MainForm'
  ClientHeight = 257
  ClientWidth = 632
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 427
    Height = 257
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object DBGrid1: TDBGrid
      Left = 0
      Top = 0
      Width = 427
      Height = 257
      Align = alClient
      DataSource = dsFiles
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      ReadOnly = True
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'NAME'
          Width = 168
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FILE_SIZE'
          Title.Caption = 'FILE_SIZE (bytes)'
          Width = 110
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ELAPSED_TIME'
          Title.Caption = 'ELAPSED_TIME (ms)'
          Width = 105
          Visible = True
        end>
    end
  end
  object Panel2: TPanel
    Left = 427
    Top = 0
    Width = 205
    Height = 257
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitLeft = 433
    object btnSelectFiles: TButton
      Left = 8
      Top = 32
      Width = 177
      Height = 25
      Caption = #1042#1099#1073#1088#1072#1090#1100' '#1092#1072#1081#1083#1099'...'
      TabOrder = 0
      OnClick = btnSelectFilesClick
    end
    object btnCopyTo: TButton
      Left = 8
      Top = 80
      Width = 177
      Height = 25
      Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100' '#1074'...'
      TabOrder = 1
      OnClick = btnCopyToClick
    end
  end
  object dsFiles: TDataSource
    DataSet = spFiles
    Left = 312
    Top = 144
  end
  object spFiles: TFDStoredProc
    Connection = dmod.Connection
    Transaction = dmod.trnRead
    UpdateOptions.AssignedValues = [uvEDelete, uvEInsert, uvEUpdate, uvCountUpdatedRecords, uvCheckRequired, uvCheckReadOnly, uvCheckUpdatable]
    UpdateOptions.EnableDelete = False
    UpdateOptions.EnableInsert = False
    UpdateOptions.EnableUpdate = False
    UpdateOptions.CountUpdatedRecords = False
    UpdateOptions.CheckRequired = False
    UpdateOptions.CheckReadOnly = False
    UpdateOptions.CheckUpdatable = False
    StoredProcName = 'FILES_SELECT'
    Left = 216
    Top = 144
    ParamData = <
      item
        Position = 1
        Name = 'ID'
        DataType = ftInteger
        ParamType = ptOutput
      end
      item
        Position = 2
        Name = 'NAME'
        DataType = ftString
        ParamType = ptOutput
        Size = 80
      end
      item
        Position = 3
        Name = 'FILE_SIZE'
        DataType = ftLargeint
        ParamType = ptOutput
      end
      item
        Position = 4
        Name = 'ELAPSED_TIME'
        DataType = ftInteger
        ParamType = ptOutput
      end>
    object spFilesID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
    end
    object spFilesNAME: TStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Size = 80
    end
    object spFilesFILE_SIZE: TLargeintField
      FieldName = 'FILE_SIZE'
      Origin = 'FILE_SIZE'
    end
    object spFilesELAPSED_TIME: TIntegerField
      FieldName = 'ELAPSED_TIME'
      Origin = 'ELAPSED_TIME'
    end
  end
end
