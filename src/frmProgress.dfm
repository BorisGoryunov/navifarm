object ProgressForm: TProgressForm
  Left = 0
  Top = 0
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'ProgressForm'
  ClientHeight = 211
  ClientWidth = 457
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object lblFileName: TLabel
    Left = 40
    Top = 21
    Width = 53
    Height = 13
    Caption = 'lblFileName'
  end
  object lbTotalSize: TLabel
    Left = 40
    Top = 135
    Width = 51
    Height = 13
    BiDiMode = bdLeftToRight
    Caption = 'lbTotalSize'
    ParentBiDiMode = False
  end
  object lbMs: TLabel
    Left = 412
    Top = 63
    Width = 21
    Height = 13
    Alignment = taRightJustify
    Caption = 'lbMs'
  end
  object lbTotalMs: TLabel
    Left = 388
    Top = 135
    Width = 45
    Height = 13
    Alignment = taRightJustify
    Caption = 'lbTotalMs'
  end
  object lbFileSize: TLabel
    Left = 40
    Top = 63
    Width = 43
    Height = 13
    Caption = 'lbFileSize'
  end
  object btnCancel: TButton
    Left = 192
    Top = 170
    Width = 75
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 0
    OnClick = btnCancelClick
  end
  object pbFiles: TProgressBar
    Left = 40
    Top = 112
    Width = 393
    Height = 17
    Step = 0
    TabOrder = 1
  end
  object pbFile: TProgressBar
    Left = 40
    Top = 40
    Width = 393
    Height = 17
    Step = 0
    TabOrder = 2
  end
  object Timer: TTimer
    Enabled = False
    Interval = 500
    OnTimer = TimerTimer
    Left = 376
    Top = 168
  end
end
