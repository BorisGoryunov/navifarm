object dmod: Tdmod
  OldCreateOrder = False
  Height = 310
  Width = 373
  object Connection: TFDConnection
    Params.Strings = (
      'CharacterSet=WIN1251'
      'Database=NAVIFARM.FDB'
      'Password=masterkey'
      'User_Name=sysdba'
      'ExtendedMetadata=False'
      'Server=localhost'
      'DriverID=FB')
    LoginPrompt = False
    Transaction = trnRead
    UpdateTransaction = trnWrite
    Left = 32
    Top = 32
  end
  object trnRead: TFDTransaction
    Options.ReadOnly = True
    Connection = Connection
    Left = 96
    Top = 32
  end
  object trnWrite: TFDTransaction
    Options.AutoStart = False
    Options.AutoStop = False
    Options.StopOptions = [xoIfCmdsInactive]
    Options.DisconnectAction = xdRollback
    Connection = Connection
    Left = 160
    Top = 32
  end
  object FDPhysFBDriverLink1: TFDPhysFBDriverLink
    Left = 64
    Top = 240
  end
  object spFilesWrite: TFDStoredProc
    Connection = Connection
    Transaction = trnWrite
    UpdateTransaction = trnWrite
    StoredProcName = 'FILES_WRITE'
    Left = 40
    Top = 120
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 248
    Top = 232
  end
  object spFilesDelete: TFDStoredProc
    Connection = Connection
    Transaction = trnWrite
    UpdateTransaction = trnWrite
    StoredProcName = 'FILES_DELETE'
    Left = 128
    Top = 120
  end
end
