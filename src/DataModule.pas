unit DataModule;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.Comp.Client, Data.DB, FireDAC.Phys.IBBase,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.VCLUI.Wait, FireDAC.Comp.UI;

type
  Tdmod = class(TDataModule)
    Connection: TFDConnection;
    trnRead: TFDTransaction;
    trnWrite: TFDTransaction;
    FDPhysFBDriverLink1: TFDPhysFBDriverLink;
    spFilesWrite: TFDStoredProc;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    spFilesDelete: TFDStoredProc;
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(AComponent:TCOmponent);override ;
    destructor Destroy;override ;
    procedure OpenDatabase ;
    procedure CloseDatabase ;
    procedure FileWrite(const AName:string ;ASize:Int64;AElapsedTime:Integer);
    procedure FileDelete();


  end;

var
  dmod: Tdmod;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ Tdmod }

procedure Tdmod.CloseDatabase;
begin
  Connection.Close ;
end;

constructor Tdmod.Create(AComponent: TCOmponent);
begin
  inherited;
  FDPhysFBDriverLink1.VendorHome:='' ;
  OpenDatabase;
end;

destructor Tdmod.Destroy;
begin
  CloseDatabase ;
  inherited;
end;

procedure Tdmod.FileDelete;
begin
  try
    trnWrite.StartTransaction;
    spFilesDelete.ExecProc ;
    trnWrite.Commit;
  finally
    if trnWrite.Active then
      trnWrite.Rollback ;
  end;
end;

procedure Tdmod.FileWrite(const AName: string; ASize: Int64;
  AElapsedTime: Integer);
begin
  spFilesWrite.Prepare;
  spFilesWrite.ParamByName('IN_NAME').AsString:=AName ;
  spFilesWrite.ParamByName('IN_SIZE').AsLargeInt:=ASize ;
  spFilesWrite.ParamByName('IN_ELAPSED').AsInteger:=AElapsedTime ;
  spFilesWrite.ExecProc;
end;

procedure Tdmod.OpenDatabase;
var
  AFilePath:string;
begin
  AFilePath:=ExtractFilePath(ParamStr(0))+'/navifarm.fdb';
  Connection.Params.Database:=AFilePath ;
  Connection.Open;
end;

end.
