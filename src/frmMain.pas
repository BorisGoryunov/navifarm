unit frmMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,FileManager, Vcl.ExtCtrls,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Vcl.Grids, Vcl.DBGrids;

type
  TMainForm = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    btnSelectFiles: TButton;
    btnCopyTo: TButton;
    DBGrid1: TDBGrid;
    dsFiles: TDataSource;
    spFiles: TFDStoredProc;
    spFilesID: TIntegerField;
    spFilesNAME: TStringField;
    spFilesFILE_SIZE: TLargeintField;
    spFilesELAPSED_TIME: TIntegerField;
    procedure btnSelectFilesClick(Sender: TObject);
    procedure btnCopyToClick(Sender: TObject);
  private
    { Private declarations }
    fFileManager:TFileManager ;
    procedure SelectFiles ;
    procedure CopyTo;
  public
    { Public declarations }
    constructor Create(AComponent:TComponent);override ;
    destructor Destroy ;override ;
  end;

var
  MainForm: TMainForm;

implementation
uses Helper , DataModule, DBManager;
{$R *.dfm}

procedure TMainForm.btnSelectFilesClick(Sender: TObject);
begin
  SelectFiles;
  CopyTo ;

end;

procedure TMainForm.CopyTo;
var
  AFolderPath:string ;
  AFile:TFile ;
  i:Integer ;
  ADbFile:TDbFile ;
  ADbFileItem:TDbFileItem ;

begin
  if fFileManager.FileItemCount=0 then
    raise Exception.Create('�������� �����.');
  
  if Helper.TDialogHelper.OpenFolder(Self, AFolderPath) then
  begin
    dmod.FileDelete ;
    fFileManager.DestinationDir:=AFolderPath;
    fFileManager.Execute(Self);
    ADbFile:=TDbFile.Create ;
    try
      for  i:= 0 to fFileManager.FileItemCount-1 do
      begin
        AFile:=fFileManager.FileItem[i];
        if AFile.IsCopied then
        begin
          ADbFileItem:=TDbFileItem.Create ;
          ADbFile.AddItem(ADbFileItem);
          ADbFileItem.Name:=AFile.Name ;
          ADbFileItem.FileSize:=AFile.FileSize ;
          ADbFileItem.ElapsedTime:=AFile.ElapsedTime;
        end;
      end;
      ADbFile.Write;
    finally
      ADbFile.Free ;
    end;
    spFiles.Close;
    spFiles.Open;
  end;
end;

constructor TMainForm.Create(AComponent: TComponent);
begin
  inherited;
  fFileManager:=TFileManager.Create;
end;

destructor TMainForm.Destroy;
begin
  fFileManager.Free ;
  inherited;
end;

procedure TMainForm.btnCopyToClick(Sender: TObject);
begin
  CopyTo  ;
end;

procedure TMainForm.SelectFiles;
var
  AFileNames:TStrings ;
  i: Integer;
begin
  AFileNames:=TStringList.Create ;
  try
    if Helper.TDialogHelper.OpenFile(Self,'c:\',AFileNames )then
    begin
      fFileManager.Initiliaze ;
      for i := 0 to AFileNames.Count-1 do
        fFileManager.AddFilePath(AFileNames[i]);
    end;
  finally
    AFileNames.Free;
  end;
end;

end.
