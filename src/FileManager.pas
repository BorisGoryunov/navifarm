unit FileManager;

interface
uses System.Classes,System.Generics.Collections,Windows,frmProgress;

  type
    TTask=class ;
    TFile=class ;
    TFileManager=class
    private
      fFiles:TList<TFile>;
      fDestinationDir:string ;
      fCancelCopy:Boolean ;
      fProgressForm:TProgressForm;
      fTask:TTask ;
      procedure OnTerminated(ASender:TObject);
      function GetTotalSize:Int64 ;
      procedure CopyFiles;
      function GetFileItem(Index: Integer): TFile;
      function GetFileItemCount: Integer;
    public
      constructor Create ;
      destructor Destroy ;override ;
      procedure Initiliaze;
      procedure AddFilePath(const AFilePath:string);
      procedure Execute(AOwner:TComponent);

      property DestinationDir:string read fDestinationDir write fDestinationDir;
      property ProgressForm:TProgressForm read fProgressForm;
      property CancelCopy:Boolean read fCancelCopy write fCancelCopy;
      property FileItem[Index:Integer]:TFile read GetFileItem ;
      property FileItemCount:Integer read GetFileItemCount ;
    end ;

    TFile=class
    private
      fPath:string ;
      fName:string ;
      fSize:Int64 ;
      fFileManager:TFileManager ;
      fElapsedTime:Integer;
      fIsCopied: Boolean;
      function GetSize: Int64;
      procedure SetPath(const Value: string);
    public
      constructor Create(AOwner:TFileManager);
      function Copy(const ANewFilePath:string):Boolean;
      function CopyToDir(const ADir: string): Boolean;
      property Path:string read fPath write SetPath  ;
      property FileManager:TFileManager read fFileManager;
      property Name:string read fName ;
      property FileSize:Int64 read fSize ;
      property ElapsedTime:Integer  read fElapsedTime ;
      property IsCopied:Boolean read fIsCopied ;
    end;
    TTask=class(TThread)
    private
    { Private declarations }
      fFileManager:TFileManager;
    protected
      procedure Execute; override;
    end;

implementation
uses  Helper,SysUtils,ShellApi,Dateutils;
{ TFileManager }
var
  AFile:TFile ;

function ProgressRoutine(TotalFileSize, TotalBytesTransferred, StreamSize,
      StreamBytesTransferred: Int64; dwStreamNumber, dwCallbackReason: DWORD;
      hSourceFile, hDestinationFile:THandle; lpData: Pointer): DWORD;stdcall ;
begin
  if AFile.FileManager.ProgressForm.Cancel then
  begin
    AFile.FileManager.CancelCopy:=true ;
    AFile.FileManager.fTask.Terminate;
    result:=PROGRESS_STOP ;
    Exit ;
  end
  else
    result:=PROGRESS_CONTINUE;

  AFile.FileManager.ProgressForm.TotalFileSize:=TotalFileSize;
  AFile.FileManager.ProgressForm.TotalBytesTransferred:=TotalBytesTransferred;
  AFile.FileManager.ProgressForm.UpdateContext;

end;

procedure TFileManager.AddFilePath(const AFilePath: string);
var
  AFile:TFile ;
begin
  AFile:=TFile.Create(Self);
  AFile.Path:=AFilePath ;
  fFiles.Add(AFile) ;
end;

procedure TFileManager.CopyFiles;
var
  AFile:TFile ;
begin
  for AFile in fFiles do
  begin
    if not AFile.CopyToDir(fDestinationDir) then
      Break ;
  end;
end;

constructor TFileManager.Create;
begin
  fFiles:=TList<TFile>.Create;
end;

destructor TFileManager.Destroy;
begin
  Helper.TListHelper.ListClear<TFile>(fFiles);
  fFiles.Free ;
  inherited;
end;

procedure TFileManager.Execute(AOwner:TComponent);
var
  AFile:TFile ;
begin

  for AFile in fFiles  do
    AFile.fIsCopied:=false;

  fCancelCopy:=false ;
  fTask:=TTask.Create(true) ;
  fTask.fFileManager:=Self ;
  fTask.OnTerminate:=OnTerminated;
  fTask.FreeOnTerminate:=true ;
  fProgressForm:=TProgressForm.Create(AOwner) ;
  try
    fProgressForm.TotalSize:=GetTotalSize ;
    fTask.Start;
    fProgressForm.ShowModal;
  finally
    fProgressForm.Free;
  end;
end;



function TFileManager.GetFileItem(Index: Integer): TFile;
begin
  result:=fFiles[index] ;
end;

function TFileManager.GetFileItemCount: Integer;
begin
  result:=fFiles.Count ;
end;


function TFileManager.GetTotalSize: Int64;
var
  ASize:Int64 ;
  AFile:TFile ;
begin
  ASize:=0 ;
  for AFile in fFiles do
  begin
    ASize:=ASize+AFile.fSize ;
  end ;
  result:=ASize ;
end;




procedure TFileManager.Initiliaze;
begin
  Helper.TListHelper.ListClear<TFile>(fFiles);
end;

procedure TFileManager.OnTerminated(ASender: TObject);
begin
  if not fCancelCopy then
    fProgressForm.Close ;
end;

{ TFile }
function TFile.CopyToDir(const ADir: string):Boolean;
var
  ANewFilePath:string ;
begin
  ANewFilePath:=ADir+'\'+fName;
  result:=Copy(ANewFilePath)   ;
end;

function TFile.Copy(const ANewFilePath: string):Boolean;
var
  a:Cardinal ;
begin
  AFile:=Self ;
  AFile.fFileManager.fProgressForm.FileName:=fName ;
  a:=GetTickCount ;
  result:=Helper.TFileHelper.CopyFile(fPath,ANewFilePath,Addr(ProgressRoutine),nil,false,0);// COPY_FILE_FAIL_IF_EXISTS);
  fElapsedTime:=GetTickCount-a ;
  fIsCopied:=result;
end;

constructor TFile.Create(AOwner:TFileManager);
begin
  fFileManager:=AOwner;
  fElapsedTime:=0 ;
end;


function TFile.GetSize: Int64;
begin
  result:=Helper.TFileHelper.GetFileSize(fPath);
end;



procedure TFile.SetPath(const Value: string);
begin
  fPath:=Value;
  fName:=ExtractFileName(Value);
  fSize:=GetSize ;
end;

{ TFileThread }

procedure TTask.Execute;
begin
  inherited;
  fFileManager.CopyFiles() ;
end;

end.
