unit frmProgress;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.ExtCtrls;

type
  TProgressForm = class(TForm)
    btnCancel: TButton;
    pbFiles: TProgressBar;
    pbFile: TProgressBar;
    lblFileName: TLabel;
    lbTotalSize: TLabel;
    lbMs: TLabel;
    lbTotalMs: TLabel;
    Timer: TTimer;
    lbFileSize: TLabel;
    procedure btnCancelClick(Sender: TObject);
    procedure TimerTimer(Sender: TObject);
  private
    fTotalSizeTransferred:Int64 ;
    fTotalSize:Int64 ;
    fTotalBytesTransferred:Int64 ;
    fTotalFileSize:Int64 ;
    fCancel:Boolean ;
    fFileName:string;
    fPrevFileTrasferred:Int64 ;
    fElapsedMs:Cardinal ;
    fTotalElapsedMs:Cardinal ;
    { Private declarations }
  public
    { Public declarations }
    constructor Create(AComponent:TComponent);override ;
    procedure UpdateContext();
    property TotalFileSize:Int64 write fTotalFileSize ;
    property TotalBytesTransferred:Int64 write fTotalBytesTransferred ;
    property Cancel:Boolean read fCancel;
    property FileName:string write fFileName ;
    property TotalSize:Int64 write fTotalSize ;
  end;


implementation
uses Helper ;
{$R *.dfm}

procedure TProgressForm.btnCancelClick(Sender: TObject);
begin
  Timer.Enabled:=false ;
  fCancel:=true ;
  Close ;
end;

constructor TProgressForm.Create(AComponent: TComponent);
begin
  inherited;
  Caption:='Копирование';
  fTotalSizeTransferred:=0;
  fPrevFileTrasferred:=0 ;
  lbMs.Caption:='';
  lbTotalMs.Caption:='';
end;



procedure TProgressForm.TimerTimer(Sender: TObject);
begin
  fTotalElapsedMs:=fTotalElapsedMs+1 ;
  lbMs.Caption:=IntToStr(GetTickCount -fElapsedMs);
  lbTotalMs.Caption:=IntToStr(GetTickCount-fTotalElapsedMs);
end;

procedure TProgressForm.UpdateContext;
begin
  if not Timer.Enabled then
  begin
    Timer.Enabled:=true ;
    fElapsedMs:=GetTickCount ;
    fTotalElapsedMs:=fElapsedMs ;
  end;

  fTotalSizeTransferred:=fPrevFileTrasferred+fTotalBytesTransferred;
  pbFile.Position := 100 * fTotalBytesTransferred div fTotalFileSize;
  pbFiles.Position:=100 * fTotalSizeTransferred div fTotalSize ;
  lblFileName.Caption:=fFileName ;
  lbFileSize.Caption:=Format('%d / %d',[fTotalBytesTransferred,fTotalFileSize]);
  lbTotalSize.Caption:=Format('%d / %d',[fTotalSizeTransferred,fTotalSize]);
  if fTotalBytesTransferred=fTotalFileSize then
  begin
    fPrevFileTrasferred:=fPrevFileTrasferred+fTotalFileSize ;
    fElapsedMs:=GetTickCount ;
  end;

end;


end.
