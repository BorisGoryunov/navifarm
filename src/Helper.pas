unit Helper;

interface
uses System.Classes,System.Generics.Collections,Forms,Windows ;
type
 TDialogHelper=class
  private
  public
    class function OpenFile(AOwner:TComponent;const AFolder:string;out AFileNames:TStrings):boolean ;overload ;
     class function OpenFolder(AOwner:TComponent;var AFolder:string):boolean ;
  end;
type
  TFileHelper=class
  public
    class function GetFileSize(const AFilePath: string): Int64;
    class function  CopyFile(const ASrcFileName, ADstFileName:string;
      AProgressRoutine:Pointer;AData:Pointer;AFlagCancelCopy:boolean;ACopyFlags:DWORD):Boolean ;
  end ;

 type
  TListHelper=class
    class procedure ListClear<T: class>(AList:TList<T>);
  end;

implementation
uses vcl.dialogs,System.Generics.defaults,  System.types , System.SysUtils;


{ TDialogHelper }

class function TDialogHelper.OpenFile(AOwner: TComponent; const AFolder: string;
  out AFileNames:TStrings): boolean;
var
  ADlg:TOpenDialog ;
begin
  ADlg:=TOpenDialog.Create(AOwner) ;
  try
    ADlg.InitialDir:=AFolder;
    ADlg.Options:=ADlg.Options+[ofAllowMultiSelect];
    result:=ADlg.Execute ;
    AFileNames.AddStrings(ADlg.Files) ;
  finally
    ADlg.Free
  end;
end;

class function TDialogHelper.OpenFolder(AOwner: TComponent;
  var AFolder: string): boolean;
var
  ADialog: TFileOpenDialog;
begin
  ADialog:=TFileOpenDialog.Create(AOwner);
  try
   ADialog.Options := ADialog.Options + [fdoPickFolders];
   ADialog.FileName:=AFolder ;
   result:=ADialog.Execute;
   AFolder := ADialog.FileName;
  finally
    ADialog.Free;
  end;
end;

{ TFileHelper }

class function TFileHelper.CopyFile(const ASrcFileName, ADstFileName: string;
  AProgressRoutine: TFNProgressRoutine; AData:Pointer; AFlagCancelCopy: Boolean;
  ACopyFlags: DWORD): boolean;
begin
  result:=CopyFileEx(PChar(ASrcFileName),PChar(ADstFileName),
      AProgressRoutine,AData,Addr(AFlagCancelCopy),
      ACopyFlags);
end;

class function TFileHelper.GetFileSize(const AFilePath: string): Int64;
var
  info: TWin32FileAttributeData;
begin
  result := -1;
  if not GetFileAttributesEx(PWideChar(AFilePath), GetFileExInfoStandard, @info) then
    Exit ;
  result := Int64(info.nFileSizeLow) or Int64(info.nFileSizeHigh shl 32);
end;

{ TListHelper }

class procedure TListHelper.ListClear<T>(AList: TList<T>);
var
  AItem:T ;
  i:Integer ;
begin
  for AItem in AList do
    AItem.Free ;
  AList.Clear() ;
end;

end.
