unit DBManager;

interface
uses System.Generics.Collections;
  type
    TDbFileItem=class;
    TDbFile=class
    private
      fItems:TList<TDbFileItem> ;

    public
      constructor Create;
      destructor Destroy ;override ;
      procedure AddItem(AItem:TDbFileItem);
      procedure Write;
  end;
  TDbFileItem=class
    private
    fName: string;
    fFileSize: Int64;
    fElapsedTime: Integer;
    public
      property Name:string read fName write fName;
      property FileSize:Int64 read fFileSize write fFileSize ;
      property ElapsedTime:Integer read fElapsedTime write fElapsedTime;
  end;
implementation

{ TDbFileItem }

uses DataModule, Helper;



{ TDbFile }

procedure TDbFile.AddItem(AItem: TDbFileItem);
begin
  fItems.Add(AItem);
end;

constructor TDbFile.Create;
begin
  fItems:=TList<TDbFileItem>.Create ;
end;

destructor TDbFile.Destroy;
begin
  Helper.TListHelper.ListClear<TDbFileItem>(fItems) ;
  fItems.Free ;
  inherited;
end;

procedure TDbFile.Write;
var
  AItem:TDbFileItem;
begin
  try
    dmod.trnWrite.StartTransaction ;
    for AItem in fItems do
      dmod.FileWrite(AItem.Name,AItem.FileSize,AItem.ElapsedTime);
    dmod.trnWrite.Commit;
  finally
    if dmod.trnWrite.Active then
      dmod.trnWrite.Rollback;
  end;
end;

end.
